<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LoanPaymentExtraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('loan_extra_payments')->insert([
            'loan_id' => 1,
            'loan_payment_id' => 3,
            'month_number' => 3,
            'amount' => 291.67,
        ]);
    }
}
