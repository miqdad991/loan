@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('My Loan') }}
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div><h3> Amount : {{ number_format($loan->amount, 0, '.', ',') }} </h3></div>
                            <div> Interest Rate : {{$loan->interest_rate}}% </div>
                            <div> Loan Term : {{$loan->loan_term}} Years </div>
                            <div> Number Of Months : {{$loan->number_of_months}} Months </div>
                            <div> Paid : {{$payments}} Months </div>
                            <div> Extra : {{$extraPayment}} Months </div>
                            <div> Rest : {{$loan->balance_number}} Months </div>
                        </div>
                        <div class="col-md-6">
                            <div><h5> Monthly Payment : {{ number_format($loan->monthly_amount, 2, '.', ',') }} </h5></div>
                            <div> Start Payment : {{$loan->start_payments}} </div>
                            <div> End Payment : {{$loan->end_payments}} </div>
                            <div>
                                <form action="/payment/store" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="loan_id" value="{{$loan->id}}">
                                    <button type="submit" class="btn btn-info" style="width: 70%;margin-top: 10px;"> Make a Payment </button>
                                </form>
                            </div>
                            <div>
                                <form action="/payment/extra" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="loan_id" value="{{$loan->id}}">
                                    <button type="submit" class="btn btn-success" style="width: 70%;margin-top: 10px;"> Make an Extra Payment </button>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-12" >
                            @if($errors->any())
                                <br>
                                <div class="alert alert-danger">* {{$errors->first()}}</div>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row justify-content-center" style="padding-top: 20px">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('Payment Schedule') }}
                </div>
                <div class="card-body">
                    <?php
                        $next_payments = $loan->start_payments;
                        $monthsWithoutExtra = $loan->number_of_months - $extraPayment;
                    ?>

                    @for($i=0; $i <= $loan->number_of_months; $i++)
                        <?php
                            $i < $payments ? $color = "#818e8c" : $color = "#66ccc3";
                            $i > $monthsWithoutExtra ? $color = "#ced2d2" : '';
                        ?>

                        <div class="scheduleDates" style="background:{{$color}};">{{$next_payments}}</div>
                        <?php $next_payments = date('Y-m-d', strtotime("+1 months", strtotime($next_payments))); ?>
                    @endfor

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
