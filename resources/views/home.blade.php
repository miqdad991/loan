@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form id="loanForm" method="POST" action="/loan" onsubmit="validateLoanForm();">
                        {{ csrf_field() }}
                        <div class="mb-3">
                            <label for="totalAmount" class="form-label">Total Amount</label>
                            <input type="number" class="form-control" name="amount" id="totalAmount" required>
                        </div>
                        <div class="mb-3">
                            <label for="interestRate" class="form-label">Annual interest rate</label>
                            <input type="number" class="form-control" name="interestRate" id="interestRate" required>
                        </div>
                        <div class="mb-3">
                            <label for="loanTerm" class="form-label">loan term</label>
                            <input type="number" class="form-control" name="loanTerm" id="loanTerm" required>
                        </div>
                        <div class="mb-3">
                            <label for="extraPayment" class="form-label">Monthly fixed extra payment</label>
                            <input type="number" class="form-control" name="extraPayment" id="extraPayment">
                        </div>
                        <button type="submit" class="btn btn-primary">Calculate My Loan</button>
                        <div id="calResutl" style="display: none">
                            Monthly Payment = <div id="monthlyPayment"></div>
                            Number Of Months = <div id="numberOfMonths"></div>
                            <button type="button" onclick="submitLoanForm()" class="btn btn-primary" id="submitForm">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/calculator.js') }}" defer></script>
@endsection
