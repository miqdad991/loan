@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('New Loan') }}</div>

                <div class="card-body">
                    <form id="loanForm" method="POST" action="/loan" onsubmit="validateLoanForm();">
                        {{ csrf_field() }}
                        <div class="row g-3 align-items-center">
                            <div class="col-md-3">
                                <label for="totalAmount" class="form-label">Total Amount</label>
                            </div>
                            <div class="col-auto">
                                <input type="number" class="form-control" name="amount" id="totalAmount" min="1000" value="{{old('amount')}}" required>
                            </div>
                            <div class="col-auto">
                            <span class="form-text">
                              * Must be integer and more than 1000
                            </span>
                            </div>
                        </div>
                        <br/>
                        <div class="row g-3 align-items-center margin-top: 1rem">
                            <div class="col-md-3">
                                <label for="interestRate" class="form-label">Annual interest rate</label>
                            </div>
                            <div class="col-auto">
                                <input type="number" class="form-control" name="interestRate" id="interestRate" min="1" max="100" value="{{old('interestRate')}}" required>
                            </div>
                            <div class="col-auto">
                            <span class="form-text">
                              %, * Minimum 1% and Maximum 100%
                            </span>
                            </div>
                        </div>
                        <br/>
                        <div class="row g-3 align-items-center margin-top: 1rem">
                            <div class="col-md-3">
                                <label for="loanTerm" class="form-label">loan term</label>
                            </div>
                            <div class="col-auto">
                                <input type="number" class="form-control" name="loanTerm" id="loanTerm" min="1" max="30" value="{{old('loanTerm')}}" required>
                            </div>
                            <div class="col-auto">
                            <span class="form-text">
                              Years, * Minimum 1 Year and Maximum 30 Years
                            </span>
                            </div>
                        </div>
                        <br/>
                        <div class="row g-3 align-items-center margin-top: 1rem">
                            <div class="col-md-3">
                                <label for="extraPayment" class="form-label">Monthly extra payment</label>
                            </div>
                            <div class="col-auto">
                                <input type="number" class="form-control" name="extraPayment" id="extraPayment" min="1" max="5" value="{{old('title_en')}}">
                            </div>
                            <div class="col-auto">
                            <span class="form-text">
                              * Optional, you can add fixed extra payment every month
                            </span>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                @if($errors->any())
                                    @foreach ($errors->all() as $error)
                                        <div>* {{ $error }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary float-end">Calculate My Loan</button>
                        <div id="calResutl" style="display: none">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>Monthly Payment</h5>
                                    <h5 id="monthlyPayment"></h5>
                                </div>
                                <div class="col-md-6">
                                    <h5>Number Of Months</h5>
                                    <h5 id="numberOfMonths"></h5>
                                </div>
                            </div>
                            <button type="button" onclick="submitLoanForm()" class="btn btn-success" id="submitForm">Submit Loan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/calculator.js') }}" defer></script>
@endsection
