<?php

namespace Database\Seeders;

use App\Models\LoanPayment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LoanPaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'loan_id' => 1,
                'amount' => 291.67
            ],
            [
                'loan_id' => 1,
                'amount' => 291.67
            ],
            [
                'loan_id' => 1,
                'amount' => 291.67
            ],
        ];
        LoanPayment::insert($data);
    }
}
