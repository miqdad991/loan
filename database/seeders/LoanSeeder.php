<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LoanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('loans')->insert([
            'user_id' => 1,
            'amount' => 5000,
            'interest_rate' => 20,
            'loan_term' => 2,
            'monthly_amount' => 291.67,
            'number_of_months' => 24,
            'monthly_extra_payments' => 3,
            'total_extra_payments' => 3,
            'balance_number' => 20,
            'start_payments' => '2023-01-01',
            'end_payments' => '2025-01-01'
        ]);
    }
}
