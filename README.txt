This Project is working on
PHP v 7.4
Laravel 8

to run this project pls follow this steps
1- git clone git@gitlab.com:miqdad991/loan.git
2- cd loan
3- composer install
4- composer update
5- open the project on the editor
6- copy the .env.example and past it in the same place
7- rename it to .enc
8- update the database config
6- go to mysql and create a database with your database name
7- return to Terminal and run `php artisan migrate`
8- run this command to insert the seed data `php artisan db:seed`
9- then run this command to generate key `php artisan key:generate`
10- run the project using this command `php artisan serve`
11- go to login page and use this information to login email="test@gmail.com" password="123456"
