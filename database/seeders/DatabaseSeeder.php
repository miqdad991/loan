<?php

namespace Database\Seeders;

use App\Models\LoanPayment;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            LoanSeeder::class,
            LoanPaymentSeeder::class,
            LoanPaymentExtraSeeder::class,
        ]);
    }
}
