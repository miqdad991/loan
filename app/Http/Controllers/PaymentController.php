<?php

namespace App\Http\Controllers;

use App\Models\Loan;
use App\Models\LoanExtraPayment;
use App\Models\LoanPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PaymentController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $loanId = $request->loan_id;
        $loan = Loan::where('id', $request->loan_id)->first();
        $payments = LoanPayment::where('loan_id', $loanId)->count();

        $payemnt_date = date('Y-m-d', strtotime("+$payments months", strtotime($loan->start_payments)));
        $date = date("Y-m-d");

        if ($payemnt_date > $date) {
            return Redirect::back()->withErrors(['msg' => 'You have to wait until '.$payemnt_date.' to make this payment']);
        }

        $loanPayment = new LoanPayment();
        $loanPayment->loan_id = $loanId;
        $loanPayment->amount = $loan->monthly_amount;
        $loanPayment->save();

        $loan->balance_number = $loan->balance_number - 1;
        $loan->save();

        return back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function extra(Request $request)
    {
        $loanId = $request->loan_id;
        $loan = Loan::where('id', $request->loan_id)->first();
        $monthlyExtraPayment = $loan->monthly_extra_payments;

        if ($monthlyExtraPayment == 0) {
            return Redirect::back()->withErrors(['msg' => 'You didnt register for any Extra Payments for this loan']);
        }

        $payments = LoanPayment::where('loan_id', $loanId)->orderByDesc('id')->first();
        if ($payments) {
            $extraPayment = LoanExtraPayment::where('loan_payment_id', $payments->id)->count();
        } else {
            return Redirect::back()->withErrors(['msg' => 'You have to make the payment first']);
        }

        if ($monthlyExtraPayment <= $extraPayment) {
            return Redirect::back()->withErrors(['msg' => 'You reached to the maximum number of Extra Payment for this month']);
        }

        $LoanExtraPayment = new LoanExtraPayment();
        $LoanExtraPayment->loan_id = $loanId;
        $LoanExtraPayment->loan_payment_id = $payments->id;
        $LoanExtraPayment->month_number = $payments->id;
        $LoanExtraPayment->amount = $loan->monthly_amount;
        $LoanExtraPayment->save();

        $endPayment = date('Y-m-d', strtotime("-1 months", strtotime($loan->end_payments)));
        $loan->total_extra_payments = $loan->total_extra_payments + 1;
        $loan->balance_number = $loan->balance_number - 1;
        $loan->end_payments = $endPayment;
        $loan->save();

        return back();

    }
}
