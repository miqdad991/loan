<?php

namespace App\Http\Controllers;

use App\Models\Loan;
use App\Models\LoanExtraPayment;
use App\Models\LoanPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $loans = Loan::where('user_id', $user_id)
            ->orderByDesc('id')
            ->get();

        return view('loan.index')->with('loans', $loans);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('loan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'amount' => 'required|integer|min:1000',
            'interestRate' => 'required|integer|min:1|max:100',
            'loanTerm' => 'required|integer|min:1|max:30'
        ]);

        $amount = $request->amount;
        $interestRate = $request->interestRate;
        $loanTerm = $request->loanTerm;
        $extraPayment = intval($request->extraPayment);

        $monthlyRate = ($interestRate / 12) / 100;
        $numberOfMonths = $loanTerm * 12;
        $monthlyAmountBeforeRate = $amount / $numberOfMonths;
        $monthlyAmount =  round($monthlyAmountBeforeRate + ($amount * $monthlyRate), 2, PHP_ROUND_HALF_UP);

        $start_payments = date("Y-m-d", strtotime(date('m', strtotime('+1 month')).'/01/'.date('Y').' 00:00:00'));;
        $end_payments = date('Y-m-d', strtotime("+$numberOfMonths months", strtotime($start_payments)));

        $loan = new Loan();
        $loan->user_id = Auth::user()->id;
        $loan->amount = $amount;
        $loan->interest_rate = $interestRate;
        $loan->loan_term = $loanTerm;
        $loan->monthly_amount = $monthlyAmount;
        $loan->number_of_months = $numberOfMonths;
        $loan->monthly_extra_payments = $extraPayment;
        $loan->balance_number = $numberOfMonths;
        $loan->start_payments = $start_payments;
        $loan->end_payments = $end_payments;
        $loan->save();

        return redirect("/loan/$loan->id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $loan = Loan::where('id', $id)->first();
        $payments = LoanPayment::where('loan_id', $id)->count();
        $extraPayments = LoanExtraPayment::where('loan_id', $id)->count();

        return view('loan.show')
            ->with('loan', $loan)
            ->with('payments', $payments)
            ->with('extraPayment', $extraPayments);
    }
}
