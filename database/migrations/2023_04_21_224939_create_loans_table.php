<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('amount');
            $table->integer('interest_rate');
            $table->integer('loan_term');
            $table->float('monthly_amount');
            $table->integer('number_of_months');
            $table->integer('monthly_extra_payments');
            $table->integer('total_extra_payments')->default(0);
            $table->integer('balance_number')->default(0);
            $table->date('start_payments');
            $table->date('end_payments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
