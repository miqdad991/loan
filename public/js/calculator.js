function validateLoanForm() {
    event.preventDefault();

    let amount = $("#totalAmount").val();
    let rate = $("#interestRate").val();
    let term = $("#loanTerm").val();

    let monthlyRate = (rate / 12) / 100;
    let numberOfMonths = term * 12;
    let monthlyAmount = amount / numberOfMonths;

    //let monthlyPayment = (amount * monthlyAmount) / (1 - (1 + monthlyRate)^(-numberOfMonths))
    let monthlyPayment = monthlyAmount + (amount * monthlyRate);
    $('#numberOfMonths').html(numberOfMonths);
    $('#monthlyPayment').html(monthlyPayment.toFixed(2));
    $('#calResutl').show();

}

function submitLoanForm() {
    $("#loanForm").submit();
}
