@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('My Loans') }}
                    <a href="{{ route('loan.create') }}" class="btn btn-dark float-end" >Add New Loan</a>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Interest Rate</th>
                            <th scope="col">Monthly Amount</th>
                            <th scope="col">Start Payment</th>
                            <th scope="col">End Payments</th>
                            <th scope="col">Details</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($loans as $loan)
                                <tr>
                                    <th>{{ $loan->id }}</th>
                                    <td class="text-success">
                                        <strong>{{ number_format($loan->amount, 0, '.', ',') }}</strong>
                                    </td>
                                    <td>{{ $loan->interest_rate }}%</td>
                                    <td class="text-primary">
                                        <strong>{{ number_format($loan->monthly_amount, 0, '.', ',') }}</strong>
                                    </td>
                                    <td>{{ $loan->start_payments }}</td>
                                    <td>{{ $loan->end_payments }}</td>
                                    <td><a href="loan/{{$loan->id}}" class="btn btn-info">Details</a> </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/calculator.js') }}" defer></script>
@endsection
